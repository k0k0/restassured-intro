package pl.javastart.restassured.test.serialization;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.javastart.main.pojo.Category;
import pl.javastart.main.pojo.Pet;
import pl.javastart.main.pojo.Tag;
import pl.javastart.restassured.test.TestBase;

import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;


public class SerializationAndDeserializationTests extends TestBase {

    @Test
    public void givenPetWhenPostPetThenPetIsCreatedTest() {

        Category category = new Category();
        category.setId(1);
        category.setName("dogs");

        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("dogs-category");

        Pet pet = new Pet();
        pet.setId(123);
        pet.setName("Burek");
        pet.setCategory(category);
        pet.setPhotoUrls(Collections.singletonList("http://photos.com/dog1.jpg"));
        pet.setTags(Collections.singletonList(tag));
        pet.setStatus("available");

        Pet actualPet = given().log().all().body(pet)
                .when().post("https://swaggerpetstore.przyklady.javastart.pl/v2/pet")
                .then().log().all().statusCode(200)
                .extract().as(Pet.class);

        Assert.assertEquals(actualPet.getId(), pet.getId(), "check ID");
        Assert.assertEquals(actualPet.getCategory().getId(), pet.getCategory().getId(), "check category ID");
        Assert.assertEquals(actualPet.getCategory().getName(), pet.getCategory().getName(), "check category name");
        Assert.assertEquals(actualPet.getPhotoUrls().get(0), pet.getPhotoUrls().get(0), "check photo URL");
        Assert.assertEquals(actualPet.getTags().get(0).getId(), pet.getTags().get(0).getId(), "check tag ID");
        Assert.assertEquals(actualPet.getTags().get(0).getName(), pet.getTags().get(0).getName(), "check tag name");
        Assert.assertEquals(actualPet.getName(), pet.getName(), "check pet name");
        Assert.assertEquals(actualPet.getStatus(), pet.getStatus(), "check pet status ");
    }

    @Test
    public void givenExistingPetIdWhenGetPetThenReturnPetTest() {

        Category category = new Category();
        category.setId(1);
        category.setName("dogs");

        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("dogs-category");

        Pet pet = new Pet();
        pet.setId(1);
        pet.setName("Azor");
        pet.setStatus("available");
        pet.setTags(Collections.singletonList(tag));
        pet.setPhotoUrls(Collections.singletonList("http://photos.com/dog1.jpg"));
        pet.setCategory(category);


        given().log().method().log().uri().body(pet)
                .when().post("https://swaggerpetstore.przyklady.javastart.pl/v2/pet")
                .then().log().all().statusCode(200);

        Pet actualPet = given().log().method().log().uri()
                .pathParam("petId", 1)
                .when().get("https://swaggerpetstore.przyklady.javastart.pl/v2/pet/{petId}")
                .then().log().all().statusCode(200)
                .extract().as(Pet.class);

        assertEquals(actualPet.getId(), pet.getId(), "Pet id");
        assertEquals(actualPet.getCategory().getId(), pet.getCategory().getId(), "Category id");
        assertEquals(actualPet.getCategory().getName(), pet.getCategory().getName(), "Category name");
        assertEquals(actualPet.getPhotoUrls().get(0), pet.getPhotoUrls().get(0), "Photo URL");
        assertEquals(actualPet.getTags().get(0).getId(), pet.getTags().get(0).getId(), "Pet tag id");
        assertEquals(actualPet.getTags().get(0).getName(), pet.getTags().get(0).getName(), "Pet tag name");
        assertEquals(actualPet.getStatus(), pet.getStatus(), "Pet status");
    }
}
