package pl.javastart.restassured.test.parameters;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.javastart.main.pojo.Category;
import pl.javastart.main.pojo.Pet;
import pl.javastart.main.pojo.Tag;
import pl.javastart.restassured.test.TestBase;

import java.util.Arrays;
import java.util.Collections;

import static io.restassured.RestAssured.given;

public class QueryParamsTests extends TestBase {

    @Test
    public void givenExistingPetWithStatusSoldWhenGetPetWithSoldStatusThenPetWithStatusIsReturnedTest() {

        Category category = new Category();
        category.setId(1);
        category.setName("cats");

        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("cats-category");

        Pet pet = new Pet();
        pet.setId(777);
        pet.setName("Mruczek");
        pet.setStatus("sold");
        pet.setTags(Collections.singletonList(tag));
        pet.setPhotoUrls(Collections.singletonList("http://photos.com/cat1.jpg"));
        pet.setCategory(category);

        given().log().method().log().uri().body(pet)
                .when().post("https://swaggerpetstore.przyklady.javastart.pl/v2/pet")
                .then().log().all().statusCode(200);

        Pet[] petsArray = given().log().method().log().uri()
                .queryParam("status", "sold")
                .when().get("https://swaggerpetstore.przyklady.javastart.pl/v2/pet/findByStatus")
                .then().log().all().statusCode(200)
                .extract().as(Pet[].class);

        Assert.assertTrue(Arrays.asList(petsArray).size() > 0);

    }
}
