package pl.javastart.restassured.test.tasks;

import org.testng.annotations.Test;
import pl.javastart.main.pojo.user.User;
import pl.javastart.restassured.test.TestBase;

import static io.restassured.RestAssured.given;

public class UserUpdateTests extends TestBase {

    @Test
    public void givenCorrectUserDataWhenFirstNameLastNameAreUpdatedThenUserDataIsUpdatedTest() {

        User user = new User();
        user.setId(445);
        user.setUsername("firstUser");
        user.setFirstName("Krzysztof");
        user.setLastName("Kowalski");
        user.setEmail("krzysztof@test.com");
        user.setPassword("password");
        user.setPhone("+123456789");
        user.setUserStatus(123);

        given().body(user)
                .when().post("user")
                .then().statusCode(200);

        user.setFirstName("Bogdan");
        user.setLastName("Smolen");

        given().body(user).pathParam("username", user.getUsername())
                .when().put("user/{username}")
                .then().statusCode(200);

        given().pathParam("username", user.getUsername())
                .when().get("user/{username}")
                .then().statusCode(200);

    }
}
